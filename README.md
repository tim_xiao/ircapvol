# irCapVol

In cap market, a cap/floor is quoted by implied volatilities but not prices. An interest rate cap volatility surface is a three-dimensional plot of the implied volatility of a cap as a function of strike and maturity. 